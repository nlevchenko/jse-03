package ru.t1.nlevchenko.tm.constant;

public class TerminalConst {

    public static final String INFO = "info";

    public static final String VERSION = "version";

    public static final String HELP = "help";
}
