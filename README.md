# TASK MANAGER

## DEVELOPER INFO

**NAME:** Nina Levchenko

**EMAIL:** veleslava97@mail.ru

## SYSTEM INFO
**OS**: Windows 10

**JDK**: Java 1.8

**RAM**: 16GB

**CPU**: i5

## BUILD PROJECT
````
mvn clean install
````

## RUN PROJECT
````
cd ./target
java -jar ./task-manager.jar
````
